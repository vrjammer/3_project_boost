﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour {

    [SerializeField]
    Vector3 motion;
    [Range(0, 3)]
    [SerializeField]
    float motionFrequency;

    float motionFactor;

    private Vector3 initialPosition;

	// Use this for initialization
	void Start () {
        initialPosition = transform.localPosition;        		
	}
	
	// Update is called once per frame
	void Update () {
        float cycle = Time.time * motionFrequency;
        const float tau = Mathf.PI * 2f;
        
        motionFactor = 0.5f * (1.0f + Mathf.Cos(cycle * tau));

        transform.localPosition = initialPosition + motion * motionFactor;
	}
}
