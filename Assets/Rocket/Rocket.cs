﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[DisallowMultipleComponent]
public class Rocket : MonoBehaviour {

    Rigidbody rigidBody;
    bool    soundPlaying;
    AudioSource audiosource;
    float startVolume;
    Vector3 cameraStartPosition;

    enum GameState { normal, dying, transcending };

    GameState gameState;

    [SerializeField] float forwardThrust;
    [SerializeField] float rcsThrust;
    [SerializeField] float stabilisationThrust;
    [SerializeField] float brakingThrust;
    [SerializeField] Camera mainCamera;
    [SerializeField] AudioSource engineSource;
    [SerializeField] AudioClip deathClip;
    [SerializeField] AudioClip finishClip;
    [SerializeField] ParticleSystem engineParticles;
    [SerializeField] ParticleSystem deathParticles;
    [SerializeField] ParticleSystem successParticles;

    // Use this for initialization
    void Start () {
        rigidBody = GetComponent<Rigidbody>();
        audiosource = GetComponent<AudioSource>();
        engineSource.Play();
        engineSource.Pause();
        startVolume = engineSource.volume;
        cameraStartPosition = mainCamera.transform.position;
        gameState = GameState.normal;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 newCameraPos = new Vector3(
            (cameraStartPosition.x + transform.position.x) * 0.5f,
            (cameraStartPosition.y + transform.position.y) * 0.5f,
            cameraStartPosition.z
            );
        mainCamera.transform.position = newCameraPos;
//        rocketParticles.transform.position = transform.position;
    }

    void FixedUpdate()
    {
        if (gameState == GameState.normal)
        {
            Thrust();
            //        RotateTransform();
            RotateRigidbody();
        }
    }

    //Use StartCoroutine();
    IEnumerator VolumeFade(AudioSource _AudioSource, float _EndVolume, float _FadeLength)
    {
        float _StartTime = Time.time;

        while (!soundPlaying &&
               Time.time < _StartTime + _FadeLength)
        {
            float alpha = (_StartTime + _FadeLength - Time.time) / _FadeLength;
            alpha = alpha * alpha;
            _AudioSource.volume = alpha * startVolume + _EndVolume * (1.0f - alpha);

            yield return null;

        }

        if (!soundPlaying && _EndVolume == 0)
        {
            _AudioSource.volume = 0;
            _AudioSource.Pause();
        }
    }

    private void CheckSoundPlaying(bool shouldPlay)
    {
        if (shouldPlay && !soundPlaying)
        {
            soundPlaying = true;
            engineSource.volume = startVolume;
            engineSource.UnPause();
            engineParticles.Play();
        }
        else if (!shouldPlay && soundPlaying)
        {
            soundPlaying = false;
            StartCoroutine(VolumeFade(engineSource, 0f, 1.0f));
            engineParticles.Stop();
        }
    }

    private void Thrust()
    {
        float forwardVelocity = Vector3.Dot(transform.TransformVector(Vector3.up), rigidBody.velocity);
        if (Input.GetKey(KeyCode.Space)) // Thrusting
        {
            rigidBody.AddRelativeForce(Vector3.up * forwardThrust * (1.0f + 1.0f / (1.0f + Mathf.Exp(forwardVelocity) ) ) );
            CheckSoundPlaying(true);
        }
        else
        {
            float braking = forwardVelocity * brakingThrust;
            rigidBody.AddRelativeForce(- Vector3.up * braking);
            CheckSoundPlaying(false);
        }
    }

    private void RotateTransform()
    {
        RigidbodyConstraints constraints = rigidBody.constraints;
        rigidBody.freezeRotation = true;
        float rotation = rcsThrust * Time.deltaTime;

        if (Input.GetKey(KeyCode.A)) // Rotate left
        {
            transform.Rotate(Vector3.forward * rotation);

        }
        else if (Input.GetKey(KeyCode.D)) // Rotate right
        {
            transform.Rotate(-Vector3.forward * rotation);
        }

        rigidBody.freezeRotation = false;
        rigidBody.constraints = constraints;
    }

    private void RotateRigidbody()
    {
        if (Input.GetKey(KeyCode.A)) // Rotate left
        {
            rigidBody.AddRelativeTorque(Vector3.forward * rcsThrust);
        }
        else if (Input.GetKey(KeyCode.D)) // Rotate right
        {
            rigidBody.AddRelativeTorque(-Vector3.forward * rcsThrust);
        }

        Vector3 angularVelocity = rigidBody.angularVelocity;
        rigidBody.AddRelativeTorque(- angularVelocity * stabilisationThrust);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (gameState == GameState.normal)
        {

            switch (collision.gameObject.tag)
            {
                case "Friendly":
                    break;
                case "Finish":
                    gameState = GameState.transcending;
                    CheckSoundPlaying(false);
                    successParticles.Play();
                    audiosource.PlayOneShot(finishClip, 1.0f);
                    StartCoroutine(LoadSceneInFiveSecs(1));
                    break;
                default:
                    gameState = GameState.dying;
                    CheckSoundPlaying(false);
                    deathParticles.Play();
                    audiosource.PlayOneShot(deathClip, 1.0f);
                    StartCoroutine(LoadSceneInFiveSecs(0));
                    break;
            }
        }
    }

    //Use StartCoroutine();
    IEnumerator LoadSceneInFiveSecs(int sceneBuildIndex)
    {
        const float fadeLength = 3.0f;
        float _StartTime = Time.time;

        while (Time.time < _StartTime + fadeLength)
        {
            yield return null;
        }

        SceneManager.LoadScene(sceneBuildIndex);
    }

}

